from tortoise import Model, fields


class Currency(Model):
    char_code = fields.CharField(
        max_length=3,
        pk=True,
    )
