from tortoise import Model, fields


class Transaction(Model):
    id = fields.IntField(pk=True)
    user = fields.ForeignKeyField(
        'models.User',
        index=True,
    )
    currency = fields.ForeignKeyField(
        'models.Currency',
    )
    tag = fields.ForeignKeyField(
        'models.Tag',
        on_delete=fields.SET_NULL,
        null=True,
    )
    amount = fields.DecimalField(
        max_digits=12,
        decimal_places=5,
    )
    name = fields.CharField(max_length=128)
    comment = fields.CharField(max_length=256, null=True)
    created_at = fields.DatetimeField(
        auto_now_add=True,
        index=True,
    )
