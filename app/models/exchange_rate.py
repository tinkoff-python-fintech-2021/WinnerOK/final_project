from tortoise import Model, fields


class ExchangeRate(Model):
    id = fields.IntField(pk=True)
    currency = fields.ForeignKeyField(
        'models.Currency',
        related_name='exchange_from',
    )
    base_currency = fields.ForeignKeyField(
        'models.Currency',
        related_name='exchange_to',
        index=True,
    )
    nominal = fields.IntField()
    value = fields.DecimalField(max_digits=12, decimal_places=5)
    last_update = fields.DatetimeField(auto_now=True)

    def __repr__(self) -> str:
        # pylint: disable=no-member
        return (
            f'{self.nominal} * <{self.currency_id}> == '  # type: ignore
            f'{self.value} * {self.base_currency_id}'
        )

    class Meta:
        unique_together = ('base_currency', 'currency')
