from tortoise import Model, fields


class Tag(Model):
    id = fields.IntField(pk=True)
    user = fields.ForeignKeyField(
        'models.User',
        index=True,
    )
    name = fields.CharField(max_length=16)
    color = fields.CharField(max_length=8)

    class Meta:
        unique_together = ('user_id', 'name')
