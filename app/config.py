from enum import Enum
from typing import Tuple, Union

from pydantic import BaseSettings, PositiveInt, PostgresDsn

from app.rates_parsers import BaseParser, RubParser


class EnvironmentEnum(str, Enum):
    PRODUCTION = 'production'
    DEVELOPMENT = 'development'
    TESTING = 'testing'


class EnvironmentSetting(BaseSettings):
    environment: EnvironmentEnum


class ProductionSettings(EnvironmentSetting):
    jwt_secret: str
    jwt_algorithm: str
    jwt_expire_min: PositiveInt
    tortoise_db_url: Union[PostgresDsn, str]
    active_rate_parsers: Tuple[BaseParser] = (RubParser(),)


class DevelopmentSettings(ProductionSettings):
    pass


class TestSettings(ProductionSettings):
    jwt_secret: str = 'abcdef'
    jwt_algorithm: str = 'HS256'
    tortoise_db_url: str = 'sqlite://:memory:'
    jwt_expire_min: PositiveInt = 24 * 60


def get_settings() -> ProductionSettings:
    current_environment = EnvironmentSetting().environment
    if current_environment == EnvironmentEnum.PRODUCTION:
        return ProductionSettings()
    if current_environment == EnvironmentEnum.DEVELOPMENT:
        return DevelopmentSettings()
    if current_environment == EnvironmentEnum.TESTING:
        return TestSettings()
    raise RuntimeError()


settings = get_settings()
