from decimal import Decimal
from typing import Dict

from asyncache import cached
from cachetools import TTLCache

from app.models import ExchangeRate
from app.schemas.currency import CurrencyCode, CurrencyExchangeRate


@cached(cache=TTLCache(ttl=3600, maxsize=30))
async def get_rates(
    base_currency_id: CurrencyCode,
) -> Dict[CurrencyCode, CurrencyExchangeRate]:
    rates = [
        CurrencyExchangeRate.from_orm(rate)
        for rate in await ExchangeRate.filter(base_currency_id=base_currency_id).all()
    ]
    return {rate.currency_id: rate for rate in rates}


def convert(exchange_rate: CurrencyExchangeRate, amount: Decimal) -> Decimal:
    return amount * exchange_rate.value / exchange_rate.nominal
