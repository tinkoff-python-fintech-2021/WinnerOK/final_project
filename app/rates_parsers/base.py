from abc import ABC, abstractmethod
from datetime import datetime
from typing import Iterator, Optional, Set

from app.schemas.currency import CurrencyCode, CurrencyRate


class BaseParser(ABC):
    @property
    @abstractmethod
    def base_currency(self) -> CurrencyCode:
        pass

    @abstractmethod
    # fixme не асинхронный, тк возникла проблема с типами в mypy
    # никакого хорошего решения, пока что не нашел =(
    def parse_rates(
        self,
        active_currencies: Set[CurrencyCode],
        base_datetime: Optional[datetime] = None,
    ) -> Iterator[CurrencyRate]:
        pass
