from datetime import datetime
from io import BytesIO
from typing import Iterator, Optional, Set

import pytz
import requests
from lxml import etree

from app.schemas.currency import CurrencyCode, CurrencyRate

from .base import BaseParser


class RubParser(BaseParser):
    base_currency = CurrencyCode('rub')
    xml_data_source = 'http://www.cbr.ru/scripts/XML_daily.asp'

    def _download_data(self) -> BytesIO:
        buffer = BytesIO()
        rv = requests.get(self.xml_data_source)
        buffer.write(rv.content)
        buffer.seek(0)
        return buffer

    def parse_rates(
        self,
        active_currencies: Set[CurrencyCode],
        base_datetime: Optional[datetime] = None,
    ) -> Iterator[CurrencyRate]:
        data = self._download_data()
        tree = etree.parse(data)
        root = tree.getroot()
        last_update_str = root.attrib['Date']
        last_update = datetime.strptime(last_update_str, '%d.%m.%Y')
        last_update = pytz.timezone('Europe/Moscow').localize(last_update, is_dst=None)

        if base_datetime is not None and last_update <= base_datetime:
            return

        for currency_element in root:
            parsed_currency = self._parse_currency(currency_element)
            if parsed_currency.currency_id in active_currencies:
                yield parsed_currency

    def _parse_currency(self, currency_element: etree.ElementBase) -> CurrencyRate:
        parsed_data = {item.tag: item.text for item in currency_element}

        return CurrencyRate(
            currency_id=parsed_data['CharCode'],
            nominal=parsed_data['Nominal'],
            value=parsed_data['Value'].replace(',', '.'),
        )
