from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise

from app.config import settings
from app.jobs.prepopulate import prepopulate_db_currencies
from app.jobs.update_rates import update_rates
from app.routers import (
    currency_router,
    tag_router,
    transaction_router,
    user_router,
    utils_router,
)
from app.security import security_router


def create_app() -> FastAPI:
    app = FastAPI()
    app.include_router(security_router)
    app.include_router(user_router)
    app.include_router(tag_router)
    app.include_router(currency_router)
    app.include_router(utils_router)
    app.include_router(transaction_router)

    register_tortoise(
        app,
        db_url=settings.tortoise_db_url,
        modules={'models': ['app.models']},
        generate_schemas=True,
        add_exception_handlers=True,
    )
    app.add_event_handler('startup', prepopulate_db_currencies)
    app.add_event_handler('startup', update_rates)
    return app
