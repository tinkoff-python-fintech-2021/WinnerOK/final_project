from typing import Set

from tortoise import transactions

from app.config import EnvironmentEnum, settings
from app.models import Currency
from app.models.exchange_rate import ExchangeRate
from app.rates_parsers import BaseParser
from app.schemas.currency import CurrencyCode


async def update_rates() -> None:
    active_currencies_code = {cur.pk for cur in await Currency.all()}
    for rate_parser in settings.active_rate_parsers:
        await update_rates_parser(rate_parser, active_currencies_code)


@transactions.atomic(
    # due to https://github.com/tortoise/tortoise-orm/issues/419
    'models'
    if settings.environment == EnvironmentEnum.TESTING
    else None
)
async def update_rates_parser(
    parser: BaseParser, active_currencies: Set[CurrencyCode]
) -> None:
    base_currency_code = parser.base_currency
    if base_currency_code not in active_currencies:
        return
    last_updated_entry = (
        await ExchangeRate()
        .filter(base_currency_id=base_currency_code)
        .only('last_update')
        .limit(1)
        .first()
    )
    last_update = (
        last_updated_entry.last_update if last_updated_entry is not None else None
    )

    for currency_rate in parser.parse_rates(active_currencies, last_update):
        await ExchangeRate.update_or_create(
            base_currency_id=base_currency_code,
            currency_id=currency_rate.currency_id,
            defaults={
                'nominal': currency_rate.nominal,
                'value': currency_rate.value,
            },
        )
