from app.models import Currency


async def prepopulate_db_currencies() -> None:
    currencies = ('kzt', 'rub', 'usd', 'eur')
    for currency_code in currencies:
        await Currency.get_or_create(char_code=currency_code)
