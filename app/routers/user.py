from fastapi import APIRouter, Depends, HTTPException, status
from tortoise.exceptions import IntegrityError

from app.models import User
from app.schemas.user import UserGet, UserPost
from app.security import get_current_user, hash_password

user_router = APIRouter(
    prefix='/users',
    tags=['users'],
)


@user_router.post('/', response_model=UserGet, status_code=status.HTTP_201_CREATED)
async def create_user(user: UserPost) -> UserGet:
    password_hash = hash_password(user.password)
    user_obj = User(username=user.username, password_hash=password_hash)
    try:
        await user_obj.save()
    except IntegrityError as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='User with given username already exists',
        ) from e
    return UserGet.from_orm(user_obj)


@user_router.get('/me', response_model=UserGet)
async def get_me(user: User = Depends(get_current_user)) -> UserGet:
    return UserGet.from_orm(user)
