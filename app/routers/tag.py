from typing import List

from fastapi import APIRouter, Depends, HTTPException, Response, status
from tortoise.exceptions import IntegrityError

from app.models import Tag, User
from app.schemas.tag import TagGet, TagPost
from app.security import get_current_user

tag_router = APIRouter(
    prefix='/tags',
    tags=['tags'],
    dependencies=[Depends(get_current_user)],
)


@tag_router.post(
    '/',
    response_model=TagGet,
    status_code=status.HTTP_201_CREATED,
)
async def create_tag(
    tag_data: TagPost,
    current_user: User = Depends(get_current_user),
) -> Tag:
    try:
        tag_obj = await Tag.create(user=current_user, **tag_data.dict())
    except IntegrityError as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Given tag name already exists',
        ) from e
    return tag_obj


@tag_router.get(
    '/',
    response_model=List[TagGet],
)
async def get_tags(
    current_user: User = Depends(get_current_user),
) -> List[Tag]:
    return await Tag.filter(user=current_user)


@tag_router.put('/{tag_id}/', response_model=TagGet)
async def put_tag(
    tag_id: int,
    tag_data: TagPost,
    current_user: User = Depends(get_current_user),
) -> Tag:
    tag_obj = await Tag.get(user=current_user, id=tag_id)
    updated_tag = await tag_obj.update_from_dict(tag_data.dict())
    await updated_tag.save()
    return updated_tag


@tag_router.delete(
    '/{tag_id}/',
    status_code=status.HTTP_204_NO_CONTENT,
    response_class=Response,
)
async def delete_tag(
    tag_id: int,
    current_user: User = Depends(get_current_user),
) -> None:
    await Tag.filter(user=current_user, id=tag_id).delete()
