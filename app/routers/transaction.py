from decimal import Decimal
from typing import Dict, List, Optional

from fastapi import APIRouter, Depends, HTTPException, Query, status

from app.models import Currency, Tag, User
from app.models.transaction import Transaction
from app.query_dependecies.common import DateRange
from app.schemas.currency import CurrencyCode
from app.schemas.tag import TagGet
from app.schemas.transaction import (
    Statistics,
    TagStatistics,
    TransactionGet,
    TransactionPost,
)
from app.security import get_current_user
from app.transaction_processing import convert, get_rates

transaction_router = APIRouter(
    prefix='/transactions',
    tags=['transactions'],
    dependencies=[Depends(get_current_user)],
)


@transaction_router.post(
    '/',
    status_code=status.HTTP_201_CREATED,
    response_model=TransactionGet,
)
async def create_transaction(
    transaction_data: TransactionPost,
    current_user: User = Depends(get_current_user),
) -> Transaction:
    currency_exist = await Currency.exists(pk=transaction_data.currency_id)
    if not currency_exist:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'Currency with code "{transaction_data.currency_id}" does not exist',
        )

    tag_exists = transaction_data.tag_id is None or await Tag.exists(
        pk=transaction_data.tag_id,
        user=current_user,
    )
    if not tag_exists:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'Tag with id "{transaction_data.tag_id}" does not exist',
        )

    created_transaction = await Transaction.create(
        user=current_user, **transaction_data.dict()
    )
    await created_transaction.fetch_related('tag')
    return created_transaction


@transaction_router.get('/', response_model=List[TransactionGet])
async def get_transaction(
    tag_id: Optional[int] = Query(
        None,
        description='Pass `-1` to get untagged transactions',
        example='10',
    ),
    date_range: DateRange = Depends(),
    current_user: User = Depends(get_current_user),
) -> List[Transaction]:
    query = Transaction.filter(
        user=current_user,
        created_at__gte=date_range.start,
        created_at__lte=date_range.end,
    ).prefetch_related('tag')

    if tag_id is not None:
        if tag_id == -1:
            tag_id = None
        query = query.filter(tag_id=tag_id)

    return await query.all()


@transaction_router.get(
    '/statistics',
)
async def get_statistics(
    date_range: DateRange = Depends(),
    current_user: User = Depends(get_current_user),
) -> Statistics:
    ranged_transactions = await (
        Transaction.filter(
            user=current_user,
            created_at__gte=date_range.start,
            created_at__lte=date_range.end,
        ).values('tag_id', 'currency_id', 'amount')
    )

    user_tags: Dict[int, TagGet] = {
        tag.id: TagGet.from_orm(tag)
        for tag in await Tag.filter(user=current_user).all()
    }

    delta_by_tag_id: Dict[Optional[int], Decimal] = {
        tag.id: Decimal('0') for tag in user_tags.values()
    }
    delta_by_tag_id[None] = Decimal('0')

    # for demonstration purposes only rubles is valid base_currency
    base_currency = CurrencyCode('rub')
    rates = await get_rates(base_currency)

    for transaction in ranged_transactions:
        currency_id = transaction['currency_id']
        transaction_amount = transaction['amount']

        if currency_id != base_currency:
            transaction_amount = convert(rates[currency_id], transaction_amount)

        delta_by_tag_id[transaction['tag_id']] += transaction_amount

    return Statistics(
        currency_id=base_currency,
        tag_stats=sorted(
            [
                TagStatistics(
                    tag=TagGet.from_orm(user_tags[tag_id]) if tag_id else None,
                    delta=transaction_amount,
                )
                for tag_id, transaction_amount in delta_by_tag_id.items()
            ],
            key=lambda x: x.delta,
            reverse=True,
        ),
    )
