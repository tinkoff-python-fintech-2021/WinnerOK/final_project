from fastapi import APIRouter

from app.jobs.update_rates import update_rates

utils_router = APIRouter(
    prefix='/utils',
    tags=['utils'],
)


@utils_router.post('/update_rates')
async def trigger_rates_update() -> None:
    await update_rates()
