from typing import List

from fastapi import APIRouter

from app.models import Currency
from app.schemas.currency import CurrencyCode, CurrencyExchangeRate
from app.transaction_processing import get_rates

currency_router = APIRouter(
    prefix='/currency',
    tags=['currency'],
)


@currency_router.get('/', response_model=List[CurrencyCode])
async def get_currencies() -> List[CurrencyCode]:
    return [cur.pk for cur in await Currency.all()]


@currency_router.get(
    '/{currency_code}/rates', response_model=List[CurrencyExchangeRate]
)
async def get_currency_rates(
    currency_code: CurrencyCode,
) -> List[CurrencyExchangeRate]:
    exchange_rates = await get_rates(currency_code)
    return list(exchange_rates.values())
