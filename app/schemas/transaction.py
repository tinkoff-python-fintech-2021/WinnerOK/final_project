from datetime import datetime
from decimal import Decimal
from typing import List, Optional

from pydantic import Field
from tortoise.contrib.pydantic import PydanticModel

from ..models import Transaction
from .currency import CurrencyCode
from .tag import TagGet


class TransactionBase(PydanticModel):
    currency_id: CurrencyCode = Field(
        ...,
        description='Transaction currency code',
        example='rub',
    )
    name: str = Field(
        ...,
        min_length=1,
        max_length=128,
        description='Transaction name',
        example='Laundry',
    )
    comment: Optional[str] = Field(
        None,
        min_length=1,
        max_length=256,
        description='Additional comment',
        example="Don't forget to ask Mike for return",
    )
    created_at: datetime = Field(
        ...,
        description='When transaction happened according to user',
        example=datetime(2021, 5, 3, 20, 39),
    )
    amount: Decimal = Field(
        ...,
        description='Transaction amount. Positive for income, negative for expenses',
        example='35.9',
    )

    class Config:
        orig_model = Transaction


class TransactionPost(TransactionBase):
    tag_id: Optional[int] = Field(
        None,
        description='ID of a tag applied attached',
        example=1,
    )


class TransactionGet(TransactionBase):
    id: int
    tag: Optional[TagGet]


class TagStatistics(PydanticModel):
    tag: Optional[TagGet]
    delta: Decimal = Field(
        ..., description='Transaction amount delta for the tag', example='-123.45'
    )


class Statistics(PydanticModel):
    currency_id: CurrencyCode = Field(
        ...,
        description='Currency code for delta in tag_stats',
        example='rub',
    )
    tag_stats: List[TagStatistics]
