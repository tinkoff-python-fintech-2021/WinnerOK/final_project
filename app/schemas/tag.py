from pydantic import Field
from tortoise.contrib.pydantic import PydanticModel

from ..models import Tag
from .utils import HexColor


class TagPost(PydanticModel):
    name: str = Field(
        ...,
        example='Food',
        min_length=1,
        max_length=16,
    )
    color: HexColor = Field(
        ...,
        example='#fab',
    )

    class Config:
        orig_model = Tag


class TagGet(TagPost):
    id: int
