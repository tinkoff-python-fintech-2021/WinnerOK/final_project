from pydantic import ConstrainedStr
from tortoise.contrib.pydantic import PydanticModel


class Username(ConstrainedStr):
    min_length = 8
    max_length = 50


class UserGet(PydanticModel):
    id: int
    username: Username


class UserPost(PydanticModel):
    username: Username
    password: str
