from pydantic.color import Color


class HexColor(Color):
    def __str__(self) -> str:
        return self.as_hex()

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Color):  # pragma: no cover
            raise NotImplementedError
        return self.as_hex() == other.as_hex()
