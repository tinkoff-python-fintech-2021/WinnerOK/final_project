from tortoise.contrib.pydantic import PydanticModel

from app.schemas.user import Username


class Token(PydanticModel):
    access_token: str
    token_type: str


class TokenData(PydanticModel):
    username: Username

    class Config:
        orm_mode = True
