from datetime import datetime
from decimal import Decimal
from typing import Any, Dict

from pydantic import ConstrainedStr, Field, validator
from tortoise.contrib.pydantic import PydanticModel

from app.models import Currency, ExchangeRate


class CurrencyCode(ConstrainedStr):
    strip_whitespace = True
    to_lower = True
    min_length = 3
    max_length = 3


class CurrencyGet(PydanticModel):
    char_code: CurrencyCode = Field(
        ...,
        description='3-letter lowercased currency code',
        example='rub',
    )

    class Config:
        orig_model = Currency


class CurrencyRate(PydanticModel):
    currency_id: CurrencyCode
    nominal: int
    value: Decimal


class CurrencyExchangeRate(CurrencyRate):
    """
    `nominal` amount of `currency` == `value` amount of `base_currency`
    """

    base_currency_id: CurrencyCode
    last_update: datetime

    @validator('base_currency_id')
    def different_base(cls, v: CurrencyCode, values: Dict[str, Any]) -> CurrencyCode:
        if v == values['currency_id']:
            raise ValueError('Base currency should not be the same as currency')
        return v

    class Config:
        orig_model = ExchangeRate
