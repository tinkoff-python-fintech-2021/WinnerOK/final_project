from datetime import date, datetime, time, timezone

from fastapi import HTTPException, Query, status


class DateRange:
    def __init__(
        self,
        start: date = Query(
            ...,
            description='Date period start',
            example=date(2021, 5, 3),
        ),
        end: date = Query(
            ...,
            description='Date period end',
            example=date(2021, 6, 7),
        ),
    ) -> None:
        if start > end:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Start should not be greater than end',
            )
        # make datetime for compatibility with databases
        self.start = datetime.combine(start, time.min, tzinfo=timezone.utc)
        self.end = datetime.combine(end, time.max, tzinfo=timezone.utc)
