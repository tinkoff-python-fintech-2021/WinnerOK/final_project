# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name
from typing import Tuple

import pytest
from fastapi import HTTPException, status
from httpx import AsyncClient, Response

from app.models import User
from app.schemas.token import Token
from app.schemas.user import UserPost
from app.security import get_current_user, hash_password, token_url, verify_password
from tests.utils import generate_token


@pytest.fixture()
def valid_pass_hash_pair():
    passwd = 'my_very_secret_pass123456'
    return passwd, hash_password(passwd)


@pytest.fixture()
def invalid_pass_hash_pair(valid_pass_hash_pair):
    passwd, _ = valid_pass_hash_pair
    return passwd, hash_password(passwd[:-1])


@pytest.fixture()
async def user_not_created_token(user_not_created) -> Tuple[User, Token]:
    user_obj, _ = user_not_created  # type: User, UserPost
    return user_obj, generate_token(user_obj)


@pytest.fixture()
async def malformed_token(user_token) -> Token:
    _, token = user_token
    token.access_token = 'fakeFakeTokenStr'
    return token


@pytest.mark.asyncio
async def test_hash_and_verify(valid_pass_hash_pair):
    passwd, passwd_hash = valid_pass_hash_pair
    assert verify_password(passwd, passwd_hash)


@pytest.mark.asyncio
async def test_invalid_hash(invalid_pass_hash_pair):
    passwd, passwd_hash_invalid = invalid_pass_hash_pair
    assert not verify_password(passwd, passwd_hash_invalid)


async def send_authentication_request(
    client: AsyncClient, user_post: UserPost
) -> Response:
    return await client.post(
        f'/{token_url}',
        data=user_post.dict() | {'grant_type': 'password'},
        headers={'content-type': 'application/x-www-form-urlencoded'},
    )


@pytest.mark.asyncio
async def test_authenticate(client: AsyncClient, user_created):
    _, user_post = user_created
    rv = await send_authentication_request(client, user_post)

    assert rv.status_code == status.HTTP_200_OK
    Token.parse_raw(rv.text)


@pytest.mark.asyncio
async def test_authenticate_fail_password(client: AsyncClient, user_created):
    _, user_post = user_created
    user_post.password = user_post.password[:-1]
    rv = await send_authentication_request(client, user_post)
    assert rv.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.asyncio
async def test_authenticate_fail_no_user(client: AsyncClient, user_not_created):
    _, user_post = user_not_created
    rv = await send_authentication_request(client, user_post)
    assert rv.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.asyncio
async def test_get_current_user(user_token):
    user_obj, user_token = user_token
    user = await get_current_user(user_token.access_token)
    assert user.pk == user_obj.pk


@pytest.mark.asyncio
async def test_get_current_user_malformed_token(user_not_created_token):
    _, nonexisting_user_token = user_not_created_token
    with pytest.raises(HTTPException):
        await get_current_user(nonexisting_user_token.access_token)
