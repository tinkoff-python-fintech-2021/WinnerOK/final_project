# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name
from typing import List, Tuple

import pytest
from fastapi import status
from fastapi.encoders import jsonable_encoder
from pydantic import parse_raw_as

from app.models import Tag
from app.schemas.tag import TagGet, TagPost
from tests.factory.tag import TagFactory


@pytest.fixture
async def tag_not_created(user_created) -> Tuple[Tag, TagPost]:
    user_obj, _ = user_created
    tag_obj = TagFactory.build(user=user_obj)
    return tag_obj, TagPost.from_orm(tag_obj)


@pytest.fixture
async def tag_created(tag_not_created) -> Tuple[Tag, TagPost]:
    tag_obj, tag_post = tag_not_created
    await tag_obj.save()
    return tag_obj, tag_post


@pytest.fixture
async def tag_created_other_user() -> Tuple[Tag, TagPost]:
    created_tag = await TagFactory.create()
    return created_tag, TagPost.from_orm(created_tag)


@pytest.mark.asyncio
async def test_tag_create(tag_not_created, authorized_client):
    tag_obj, tag_post = tag_not_created  # type: Tag, TagPost
    rv = await authorized_client.post('/tags', json=jsonable_encoder(tag_post))

    assert rv.status_code == status.HTTP_201_CREATED
    assert await tag_obj.exists()


@pytest.mark.asyncio
async def test_tag_already_exists(tag_created, authorized_client):
    tag_obj, tag_post = tag_created  # type: Tag, TagPost
    rv = await authorized_client.post('/tags', json=jsonable_encoder(tag_post))

    assert rv.status_code == status.HTTP_400_BAD_REQUEST
    db_object = await Tag.get(pk=tag_obj.pk)
    assert tag_obj == db_object


@pytest.mark.asyncio
async def test_create_tag_other_user_has(tag_created_other_user, authorized_client):
    _, tag_post = tag_created_other_user  # type: Tag, TagPost
    rv = await authorized_client.post('/tags', json=jsonable_encoder(tag_post))

    assert rv.status_code == status.HTTP_201_CREATED
    assert await Tag.all().count() == 2


@pytest.mark.asyncio
@pytest.mark.usefixtures('tag_created_other_user')
async def test_get_tags(authorized_client, tag_created):
    current_user_tag, _ = tag_created

    rv = await authorized_client.get('/tags')

    assert rv.status_code == status.HTTP_200_OK
    returned_data = parse_raw_as(List[TagGet], rv.text)

    assert len(returned_data) == 1
    expected_obj = TagGet.from_orm(current_user_tag)
    assert returned_data[0] == expected_obj


@pytest.mark.asyncio
async def test_delete_own_tag(authorized_client, tag_created):
    user_tag, _ = tag_created

    rv = await authorized_client.delete(f'/tags/{user_tag.id}')

    assert rv.status_code == status.HTTP_204_NO_CONTENT
    assert not await user_tag.exists()


@pytest.mark.asyncio
async def test_delete_other_tag(tag_created_other_user, authorized_client):
    other_user_tag, _ = tag_created_other_user

    rv = await authorized_client.delete(f'/tags/{other_user_tag.id}')

    assert rv.status_code == status.HTTP_204_NO_CONTENT
    assert await other_user_tag.exists()


@pytest.mark.asyncio
async def test_delete_nonexisting_tag(authorized_client):
    rv = await authorized_client.delete('/tags/1')

    assert rv.status_code == status.HTTP_204_NO_CONTENT
    assert await Tag.all().count() == 0


@pytest.mark.asyncio
async def test_put_tag(authorized_client, tag_created):
    user_tag, _ = tag_created
    updated_tag = TagFactory.build(user=user_tag.user)

    rv = await authorized_client.put(
        f'/tags/{user_tag.id}',
        json=jsonable_encoder(TagPost.from_orm(updated_tag)),
    )

    assert rv.status_code == status.HTTP_200_OK
    returned_object = TagGet.parse_raw(rv.text)

    updated_tag.id = user_tag.id
    expected_object = TagGet.from_orm(updated_tag)
    assert returned_object == expected_object


@pytest.mark.asyncio
async def test_put_nonexisting_tag(authorized_client, tag_not_created):
    _, tag_post = tag_not_created

    rv = await authorized_client.put(
        '/tags/1',
        json=jsonable_encoder(tag_post),
    )

    assert rv.status_code == status.HTTP_404_NOT_FOUND
