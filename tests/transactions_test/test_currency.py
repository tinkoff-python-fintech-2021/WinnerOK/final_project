# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name
from typing import List, Set

import pytest
from fastapi import status
from pydantic import parse_raw_as

from app.models import Currency
from app.schemas.currency import CurrencyExchangeRate


@pytest.fixture()
async def created_currency_codes(created_currency: List[Currency]) -> Set[str]:
    return {cur.pk for cur in created_currency}


@pytest.mark.asyncio
async def test_get_currencies(created_currency_codes, client):
    rv = await client.get('/currency')

    assert rv.status_code == status.HTTP_200_OK
    assert set(rv.json()) == created_currency_codes


@pytest.mark.asyncio
async def test_get_exchange_rates(currency_exchange_rates, client):
    base_currency, rates = currency_exchange_rates
    rv = await client.get(f'/currency/{base_currency.pk}/rates')

    assert rv.status_code == status.HTTP_200_OK
    returned_rates = parse_raw_as(List[CurrencyExchangeRate], rv.text)
    assert returned_rates == [CurrencyExchangeRate.from_orm(rate) for rate in rates]
