# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name
from typing import List

import pytest
from fastapi import status
from fastapi.encoders import jsonable_encoder
from httpx import AsyncClient, Response
from pydantic import parse_raw_as

from app.models import Transaction
from app.schemas.transaction import TransactionGet, TransactionPost


async def send_transaction_post(
    authorized_client: AsyncClient,
    transaction_obj: Transaction,
) -> Response:
    rv = await authorized_client.post(
        '/transactions',
        json=jsonable_encoder(
            TransactionPost.from_orm(transaction_obj),
        ),
    )
    return rv


async def assert_transaction_created(
    response: Response,
    transaction_obj: Transaction,
) -> bool:
    assert response.status_code == status.HTTP_201_CREATED
    assert await transaction_obj.exists()
    persisted_obj = await transaction_obj.get()
    assert TransactionGet.parse_raw(
        response.text
    ) == await TransactionGet.from_tortoise_orm(persisted_obj)
    return True


@pytest.mark.asyncio
async def test_transaction_create(
    authorized_client, transaction_not_created: Transaction
):
    rv = await send_transaction_post(
        authorized_client,
        transaction_not_created,
    )
    assert await assert_transaction_created(rv, transaction_not_created)


@pytest.mark.asyncio
async def test_transaction_create_untagged(
    authorized_client, transaction_untagged_not_created: Transaction
):
    rv = await send_transaction_post(
        authorized_client,
        transaction_untagged_not_created,
    )
    assert await assert_transaction_created(rv, transaction_untagged_not_created)


@pytest.mark.asyncio
async def test_transaction_create_invalid_currency(
    authorized_client, transaction_not_created: Transaction
):
    transaction_not_created.currency_id = 'qqq'  # type: ignore

    rv = await send_transaction_post(
        authorized_client,
        transaction_not_created,
    )

    assert rv.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.asyncio
async def test_transaction_create_invalid_tag(
    authorized_client, transaction_not_created: Transaction
):
    transaction_not_created.tag_id = -1  # type: ignore

    rv = await send_transaction_post(
        authorized_client,
        transaction_not_created,
    )

    assert rv.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.asyncio
async def test_retrieve_all_transactions(
    authorized_client,
    all_time_transactions,
):
    date_range, transactions = all_time_transactions
    rv = await authorized_client.get(
        '/transactions',
        params={
            'start': date_range.start.date(),
            'end': date_range.end.date(),
        },
    )
    assert rv.status_code == status.HTTP_200_OK
    retrieved_objects = parse_raw_as(List[TransactionGet], rv.text)
    expected_objects = [TransactionGet.from_orm(tr) for tr in transactions]

    retrieved_objects.sort(key=lambda x: x.id)
    expected_objects.sort(key=lambda x: x.id)
    assert retrieved_objects == expected_objects
