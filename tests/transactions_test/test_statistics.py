# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name
from decimal import Decimal

import pytest
from fastapi import status

# Pylint says that Currency is unused, but it is import for mypy typechecking
from app.models import (  # pylint: disable=unused-import
    Currency,
    ExchangeRate,
    Transaction,
)
from app.schemas.currency import CurrencyCode, CurrencyExchangeRate
from app.schemas.tag import TagGet
from app.schemas.transaction import Statistics, TagStatistics
from app.transaction_processing import convert


async def transform_transaction(
    base_cur_code: CurrencyCode, transaction: Transaction
) -> Decimal:
    transaction_currency_code = transaction.currency_id  # type: ignore
    if transaction_currency_code == base_cur_code:
        return transaction.amount

    exchange_rate = await ExchangeRate.get(
        base_currency_id=base_cur_code,
        currency_id=transaction_currency_code,
    )

    return convert(
        CurrencyExchangeRate.from_orm(exchange_rate),
        transaction.amount,
    )


@pytest.fixture
async def full_stat_rub(all_time_transactions, currency_exchange_rates):
    date_range, transactions = all_time_transactions
    base_currency, _ = currency_exchange_rates  # type: Currency, list[ExchangeRate]
    tag_statistics = sorted(
        [
            TagStatistics(
                tag=(TagGet.from_orm(transaction.tag) if transaction.tag else None),
                delta=await transform_transaction(
                    CurrencyCode(base_currency.char_code), transaction
                ),
            )
            for transaction in transactions
        ],
        key=lambda x: x.delta,
        reverse=True,
    )
    resulting_statistics = Statistics(
        currency_id=base_currency.char_code,  # fixme: сделать более generic, при расширении кода
        tag_stats=tag_statistics,
    )
    return date_range, resulting_statistics


@pytest.mark.asyncio
async def test_get_statistics(full_stat_rub, authorized_client):
    date_range, expected_statistics = full_stat_rub

    rv = await authorized_client.get(
        '/transactions/statistics',
        params={
            'start': date_range.start.date(),
            'end': date_range.end.date(),
        },
    )

    assert rv.status_code == status.HTTP_200_OK
    retrieved_statistics = Statistics.parse_raw(rv.text)
    assert retrieved_statistics == expected_statistics
