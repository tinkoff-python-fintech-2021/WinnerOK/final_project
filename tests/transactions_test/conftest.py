# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name
from typing import List, Tuple

import pytest

from app.models import Currency, ExchangeRate, Tag, Transaction
from app.query_dependecies.common import DateRange
from tests.factory.currency import CurrencyFactory
from tests.factory.exchange_rate import ExchangeRateFactory
from tests.factory.tag import TagFactory
from tests.factory.transaction import TransactionFactory


@pytest.fixture
def base_currency(created_currency) -> Currency:
    return created_currency[0]


@pytest.fixture
async def tag_created(user_created) -> Tag:
    user_obj, _ = user_created
    return await TagFactory.create(user=user_obj)


@pytest.fixture
def transaction_not_created(tag_created, base_currency, user_created) -> Transaction:
    user_obj, _ = user_created
    return TransactionFactory.build(
        user=user_obj,
        tag=tag_created,
        currency=base_currency,
    )


@pytest.fixture
def transaction_untagged_not_created(base_currency, user_created) -> Transaction:
    user_obj, _ = user_created
    return TransactionFactory.build(
        user=user_obj,
        tag=None,
        currency=base_currency,
    )


@pytest.fixture
async def transaction_created(transaction_not_created) -> Transaction:
    await transaction_not_created.save()
    return transaction_not_created


@pytest.fixture
async def transaction_untagged_created(transaction_untagged_not_created) -> Transaction:
    await transaction_untagged_not_created.save()
    return transaction_untagged_not_created


@pytest.fixture
def all_time_transactions(
    transaction_untagged_created: Transaction,
    transaction_created: Transaction,
) -> Tuple[DateRange, List[Transaction]]:
    no_tag_trans_created_at = transaction_untagged_created.created_at
    full_trans_created_at = transaction_created.created_at
    if no_tag_trans_created_at > full_trans_created_at:
        return (
            DateRange(
                start=full_trans_created_at,
                end=no_tag_trans_created_at,
            ),
            [transaction_created, transaction_untagged_created],
        )

    return (
        DateRange(
            start=no_tag_trans_created_at,
            end=full_trans_created_at,
        ),
        [transaction_untagged_created, transaction_created],
    )


@pytest.fixture()
async def created_currency() -> List[Currency]:
    currency_codes = ('rub', 'kzt', 'usd', 'eur')
    return [await CurrencyFactory.create(char_code=code) for code in currency_codes]


@pytest.fixture()
async def currency_exchange_rates(
    created_currency: List[Currency],
) -> Tuple[Currency, List[ExchangeRate]]:
    base_currency = created_currency[0]
    rates = []
    for currency in created_currency:
        if base_currency != currency:
            created_rate = await ExchangeRateFactory.create(
                base_currency=base_currency,
                currency=currency,
            )
            rates.append(created_rate)
    currency_count = len(created_currency)
    assert await ExchangeRate.all().count() == (currency_count - 1)
    return base_currency, rates
