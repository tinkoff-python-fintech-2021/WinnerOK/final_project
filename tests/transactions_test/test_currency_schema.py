# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name

from datetime import datetime
from decimal import Decimal

import pytest
import pytz

from app.schemas.currency import CurrencyCode, CurrencyExchangeRate


@pytest.fixture()
def base_exchange_rate_data():
    return {
        'nominal': 1,
        'value': Decimal('42'),
        'last_update': pytz.utc.localize(datetime.utcnow()),
    }


def test_same_currency_exchange_rate(base_exchange_rate_data):
    currency_id = CurrencyCode('kzt')
    with pytest.raises(ValueError):
        CurrencyExchangeRate(
            currency_id=currency_id,
            base_currency_id=currency_id,
            **base_exchange_rate_data,
        )


def test_different_currency_exchange_rate(base_exchange_rate_data):
    currency_id = CurrencyCode('kzt')
    other_currency_id = CurrencyCode('rub')
    CurrencyExchangeRate(
        currency_id=currency_id,
        base_currency_id=other_currency_id,
        **base_exchange_rate_data,
    )
