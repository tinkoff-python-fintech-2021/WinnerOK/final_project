# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name
from typing import Tuple

import pytest
from httpx import AsyncClient
from tortoise.contrib.test import finalizer, initializer

from app.config import EnvironmentEnum, settings
from app.main import create_app
from app.models import User
from app.schemas.token import Token
from app.schemas.user import UserPost
from tests.factory.user import UserFactory
from tests.utils import generate_token, get_authorization_header


@pytest.fixture(autouse=True, scope='session')
def assert_test_environment():
    assert (
        settings.environment == EnvironmentEnum.TESTING
    ), 'Tests should only be run in testing environment'


@pytest.fixture(autouse=True)
def db():
    initializer(['app.models'], db_url=settings.tortoise_db_url, app_label='models')
    yield
    finalizer()


@pytest.fixture()
def application():
    return create_app()


@pytest.fixture()
# return type is ignored because mypy
# insists that it should be `AsyncGenerator` but pytest handles its differently
async def client(application) -> AsyncClient:  # type: ignore
    async with AsyncClient(app=application, base_url='http://test') as ac:
        yield ac


@pytest.fixture()
def user_not_created() -> Tuple[User, UserPost]:
    user_obj = UserFactory.build()
    user_model = UserPost(username=user_obj.username, password='test')
    return user_obj, user_model


@pytest.fixture()
async def user_created(user_not_created) -> Tuple[User, UserPost]:
    user_obj, user_model = user_not_created
    await user_obj.save()
    return user_obj, user_model


@pytest.fixture()
async def user_token(user_created) -> Tuple[User, Token]:
    user_obj, _ = user_created  # type: User, UserPost
    return user_obj, generate_token(user_obj)


@pytest.fixture()
async def authorized_client(user_token, application) -> AsyncClient:  # type: ignore
    _, token = user_token
    async with AsyncClient(
        app=application, base_url='http://test', headers=get_authorization_header(token)
    ) as ac:
        yield ac
