from typing import Dict

from app.models import User
from app.schemas.token import Token, TokenData
from app.security import create_access_token


def generate_token(user_obj: User) -> Token:
    token_data = TokenData.from_orm(user_obj)
    access_token = create_access_token(token_data)
    return Token(access_token=access_token, token_type='bearer')


def get_authorization_header(token: Token) -> Dict[str, str]:
    return {'Authorization': f'{token.token_type} {token.access_token}'}
