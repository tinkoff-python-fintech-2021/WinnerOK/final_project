import pytest
from fastapi import status
from httpx import AsyncClient

from app.models import User
from app.schemas.user import UserGet
from tests.utils import get_authorization_header


@pytest.mark.asyncio
async def test_create_user(client: AsyncClient, user_not_created):
    _, user_model = user_not_created
    rv = await client.post(
        '/users',
        json=user_model.dict(),
    )

    assert rv.status_code == status.HTTP_201_CREATED
    assert User.exists(username=user_model.username)


@pytest.mark.asyncio
async def test_create_user_repeated(client: AsyncClient, user_created):
    _, user_model = user_created
    rv = await client.post(
        '/users',
        json=user_model.dict(),
    )

    assert rv.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.asyncio
async def test_get_me(client: AsyncClient, user_token):
    user_obj, token = user_token
    rv = await client.get(
        '/users/me',
        headers=get_authorization_header(token),
    )

    assert rv.status_code == status.HTTP_200_OK
    retrieved_user = UserGet.parse_raw(rv.text)
    assert retrieved_user == UserGet.from_orm(user_obj)
