# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name

import pytest

from app.jobs.update_rates import update_rates
from app.models import ExchangeRate


@pytest.mark.asyncio
@pytest.mark.usefixtures('available_currencies')
async def test_update_rates(mock_datasources):
    assert await ExchangeRate.all().count() == 0

    await update_rates()

    created_rates = await ExchangeRate.all()
    assert len(created_rates) == len(mock_datasources)

    for expected_rate in mock_datasources:
        assert await expected_rate.exists()
