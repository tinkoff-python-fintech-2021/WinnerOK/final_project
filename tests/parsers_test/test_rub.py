# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name
from datetime import datetime

import pytest
import pytz

from app.rates_parsers import RubParser


@pytest.fixture()
def filtered_mock_rub_datasource(mock_rub_datasource):
    expected_results = mock_rub_datasource[1:]
    active_currencies = {res.currency_id for res in expected_results}
    return active_currencies, expected_results


def test_base_currency():
    assert RubParser.base_currency == 'rub'


def test_data_parse(filtered_mock_rub_datasource):
    active_currencies, expected_results = filtered_mock_rub_datasource

    parser = RubParser()
    parsed_rates = list(parser.parse_rates(active_currencies, None))

    assert parsed_rates == expected_results


def test_outdated_data_parse(filtered_mock_rub_datasource):
    active_currencies, _ = filtered_mock_rub_datasource
    last_update = pytz.utc.localize(datetime.max)

    parser = RubParser()
    parsed_rates = list(parser.parse_rates(active_currencies, last_update))

    assert len(parsed_rates) == 0
