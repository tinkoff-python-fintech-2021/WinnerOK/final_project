# pylint: disable=redefined-outer-name
# disable "redefining" fixtures name
from decimal import Decimal
from pathlib import Path
from typing import List, Set

import pytest
from requests_mock import Mocker

from app.models import ExchangeRate
from app.rates_parsers import RubParser
from app.schemas.currency import CurrencyCode, CurrencyRate
from tests.factory.currency import CurrencyFactory


@pytest.fixture
def tests_files_folder() -> Path:
    path_to_current_file = Path(__file__).resolve()
    current_directory = path_to_current_file.parent
    files_folder = current_directory / 'files'
    return files_folder


@pytest.fixture()
def mock_rub_datasource(
    tests_files_folder, requests_mock: Mocker
) -> List[CurrencyRate]:
    data_source_file = tests_files_folder / 'ru_feed.xml'
    with data_source_file.open('rb') as f:
        requests_mock.get(RubParser.xml_data_source, content=f.read())

    expected_results = [
        CurrencyRate(
            currency_id=CurrencyCode('usd'), nominal=1, value=Decimal('74.8451')
        ),
        CurrencyRate(
            currency_id=CurrencyCode('eur'), nominal=1, value=Decimal('90.5850')
        ),
        CurrencyRate(
            currency_id=CurrencyCode('kzt'), nominal=100, value=Decimal('17.4178')
        ),
    ]
    return expected_results


@pytest.fixture()
async def available_currencies() -> Set[CurrencyCode]:
    parsing_currencies = [
        await CurrencyFactory.create(char_code='rub'),
        await CurrencyFactory.create(char_code='usd'),
    ]
    return {CurrencyCode(cur.char_code) for cur in parsing_currencies}


@pytest.fixture()
async def mock_datasources(
    available_currencies,
    mock_rub_datasource,
) -> List[ExchangeRate]:
    datasets = (('rub', mock_rub_datasource),)
    expected_rates = []

    for currency_code, currency_dataset in datasets:
        expected_rates.extend(
            [
                ExchangeRate(
                    base_currency_id=currency_code,
                    **currency_rate.dict(),
                )
                for currency_rate in currency_dataset
                if currency_rate.currency_id in available_currencies
            ]
        )

    return expected_rates
