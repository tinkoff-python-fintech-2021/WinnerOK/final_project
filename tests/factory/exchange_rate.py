from datetime import datetime

import factory
import pytz

from app.models import ExchangeRate
from tests.factory.currency import CurrencyFactory

from .async_base import AsyncFactory


class ExchangeRateFactory(AsyncFactory):
    class Meta:
        model = ExchangeRate

    currency = factory.SubFactory(CurrencyFactory)
    base_currency = factory.SubFactory(CurrencyFactory)
    nominal = factory.Faker('pyint', min_value=1, max_value=150)
    value = factory.Faker('pydecimal', positive=True, max_value=150)
    last_update = factory.LazyAttribute(
        lambda o: datetime.combine(o.date, o.time, pytz.utc)
    )

    class Params:
        date = factory.Faker('date_object')
        time = factory.Faker('time_object')
