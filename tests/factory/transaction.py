from datetime import datetime

import factory
import pytz

from app.models import Transaction
from tests.factory.currency import CurrencyFactory

from .async_base import AsyncFactory
from .tag import TagFactory
from .user import UserFactory


class TransactionFactory(AsyncFactory):
    class Meta:
        model = Transaction

    user = factory.SubFactory(UserFactory)
    currency = factory.SubFactory(CurrencyFactory)
    tag = factory.SubFactory(TagFactory)
    amount = factory.Faker('pydecimal', min_value=-150, max_value=150)
    name = factory.Faker('text', max_nb_chars=128)
    comment = factory.Faker('text', max_nb_chars=256)
    created_at = factory.LazyAttribute(
        lambda o: datetime.combine(o.date, o.time, pytz.utc)
    )

    class Params:
        date = factory.Faker('date_object')
        time = factory.Faker('time_object')
