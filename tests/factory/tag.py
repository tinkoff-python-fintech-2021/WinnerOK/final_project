import factory

from app.models import Tag
from tests.factory.user import UserFactory

from .async_base import AsyncFactory


class TagFactory(AsyncFactory):
    class Meta:
        model = Tag

    user = factory.SubFactory(UserFactory)
    name = factory.Faker('safe_color_name')
    color = factory.Faker('color')
