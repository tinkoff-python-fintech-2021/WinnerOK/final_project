import factory

from app.models import Currency

from .async_base import AsyncFactory


class CurrencyFactory(AsyncFactory):
    class Meta:
        model = Currency

    char_code = factory.LazyAttribute(lambda o: o.currency_code.lower())

    class Params:
        currency_code = factory.Faker('currency_code')
