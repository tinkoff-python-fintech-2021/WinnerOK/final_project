from factory.fuzzy import FuzzyText

from app.models import User
from app.security import hash_password

from .async_base import AsyncFactory


class UserFactory(AsyncFactory):
    class Meta:
        model = User

    username = FuzzyText(length=10)
    # hardcoded password because I want to authorize with created user
    password_hash = hash_password('test')
